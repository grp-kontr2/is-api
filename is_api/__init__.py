import is_api.log_config
from .client import IsApiClient

__version__ = '0.2'

is_api.log_config.load_config()
