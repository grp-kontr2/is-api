# IS MUNI API python wrapper


## Getting Started



### Prerequisites

- requires Python 3.6 or higher

### Install

- Using PIP:
[source, sh]
----
pip install https://gitlab.fi.muni.cz/grp-kontr2/is-api.git
----

- Using Pipenv
[source, sh]
----
pipenv install https://gitlab.fi.muni.cz/grp-kontr2/is-api.git
----

## Examples

[source, python]
----
# TBD
----

## Development

TBD


